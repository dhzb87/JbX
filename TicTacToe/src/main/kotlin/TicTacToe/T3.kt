package TicTacToe

import java.util.*
import java.util.function.IntUnaryOperator
import kotlin.math.abs


class T3 {

    var board = IntArray(9)
    var history = IntArray(9)
    var counter = 0

    fun move(pos: Int) {
        board[pos] = if (counter % 2 == 0) +1 else -1
        history[counter++] = pos
    }

    fun undo() {
        if (counter != 0) board[history[--counter]] = 0
    }

    fun undoAll() {
        while (counter > 0) undo()
    }

    fun isValidMove(pos: Int): Boolean {
        if (pos < 0 || pos > 8) return false
        if (board[pos] !== 0) return false
        return !threeInARow()
    }

    private fun threeInARow(): Boolean {
        val rows = arrayOf<IntArray>(intArrayOf(0, 1, 2), intArrayOf(3, 4, 5), intArrayOf(6, 7, 8), intArrayOf(0, 3, 6), intArrayOf(1, 4, 7), intArrayOf(2, 5, 8), intArrayOf(0, 4, 8), intArrayOf(2, 4, 6)) // diagonal
        return Arrays.stream(rows).parallel().anyMatch { row: IntArray? ->
            val sum: Int = Arrays.stream(row).map(IntUnaryOperator { pos: Int -> board[pos] }).sum()
            abs(sum) == 3
        }
    }
    override fun toString(): String {
        val sym = charArrayOf('O', ' ', 'X')
        val repr = CharArray(9)
        for (i in board.indices) repr[i] = sym[board[i] + 1]
        return "<tr>\n" +
                    String.format("  <td onclick=\"sendMove(0)\">%c</td>\n", repr[0]) +
                    String.format("  <td onclick=\"sendMove(1)\">%c</td>\n", repr[1]) +
                    String.format("  <td onclick=\"sendMove(2)\">%c</td>\n", repr[2]) +
                "</tr>\n" +
                "<tr>\n" +
                    String.format("  <td onclick=\"sendMove(3)\">%c</td>\n", repr[3]) +
                    String.format("  <td onclick=\"sendMove(4)\">%c</td>\n", repr[4]) +
                    String.format("  <td onclick=\"sendMove(5)\">%c</td>\n", repr[5]) +
                "</tr>\n" +
                "<tr>\n" +
                    String.format("  <td onclick=\"sendMove(6)\">%c</td>\n", repr[6]) +
                    String.format("  <td onclick=\"sendMove(7)\">%c</td>\n", repr[7]) +
                    String.format("  <td onclick=\"sendMove(8)\">%c</td>\n", repr[8]) +
                "</tr>\n"
    }


}