IntStream qsort(IntStream numbers) {
    IntStream.Builder smaller = IntStream.builder();
    IntStream.Builder greater = IntStream.builder();
    OptionalInt pivot = numbers.reduce((x,y) -> {
        if (y <= x) smaller.accept(y);
        else greater.accept(y);
        return x;
    });
    if (pivot.isEmpty()) return IntStream.empty();
    return IntStream.concat(qsort(smaller.build()),
                            IntStream.concat(IntStream.of(pivot.getAsInt()),
                                             qsort(greater.build())));
}
