import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Random;
import java.util.function.Predicate;
import java.util.stream.Stream;

/* Der Code ist ein Beispiel, der viele Eigenschaften für
   einen modernen Programmierstil zusammenbringt.
   In Java 21/22 starten mit: jshell --enable-preview
*/

enum Move { LEFT, RIGHT; }

record Transition(String fromState, char read, char write, Move move, String toState) {
    @Override public String toString() {
        return STR."(\{fromState}, \{read}) -> (\{write}, \{move}, \{toState})";
    }
    public static Optional<Transition> find(List<Transition> transitions, Predicate<Transition> matcher, Function<List<Transition>, Transition> selector) {
        List<Transition> matches = transitions.stream().filter(matcher).toList();
        return switch (matches.size()) {
            case 0  -> Optional.empty();
            case 1  -> Optional.<Transition>of(matches.get(0));
            default -> Optional.<Transition>of(selector.apply(matches));
        };
    }
    public static Optional<Transition> find(List<Transition> transitions, Predicate<Transition> predicate) {
        return find(transitions, predicate, list -> list.get(new Random().nextInt(list.size())));
    }
    public static Predicate<Transition> matcher(String fromState, char read) {
        return t -> t.fromState().equals(fromState) && t.read() == read;
    }
}

class Cell { // LinkedCell
    Cell left, right;
    char defaultValue, value;

    public Cell(char defaultValue) {
        value = this.defaultValue = defaultValue;
    }
    public Cell move(Move move) {
        Cell cell = null;
        switch(move) {
            case RIGHT:
                if (right != null) cell = right;
                else right = cell = new Cell(defaultValue);
                cell.left = (left == null && value == defaultValue) ? null : this;
                break;
            case LEFT:
                if (left != null) cell = left;
                else left = cell = new Cell(defaultValue);
                cell.right = (right == null && value == defaultValue) ? null : this;
                break;
        }
        return cell;
    }
    public Cell slide(int n, Move move) {
        if (n == 0) return this;
        return this.move(move).slide(n - 1, move);
    }
    public char read() {
        return value;
    }
    public Cell write(char value) {
        this.value = value; return this;
    }
    public Cell init(String init) {
        Cell cell = this;
        for(char c : init.toCharArray()) {
            cell = cell.write(c).move(Move.RIGHT);
        }
        return cell;
    }
    @Override public String toString() {
        String s = "";
        Cell c = this;
        while(c.left != null) c = c.left;
        do {
            s += (c == this ? "<" : " " ) + c.value + (c == this ? ">" : " ");
            c = c.right;
        } while (c != null);
        return s;
    }
}

class Env {
    private String state;
    private Cell cell;
    private int counter = 0;
    public Env(Cell cell, String state) {
        this.cell = cell;
        this.state = state;
    }
    public Env update(Cell cell, String state) {
        counter++;
        this.cell = cell;
        this.state = state;
        return this;
    }
    public String getState() { return state; }
    public Cell getCell() { return cell; }
    @Override public String toString() {
        return String.format("%3d: %s [%s]", counter, cell, state);
    }
}

class TM {
    final List<Transition> transitions;
    public TM(List<Transition> transitions) {
        this.transitions = transitions;
    }
    public Env step(Env env) {
        String state = env.getState();
        Cell  cell  = env.getCell(); 
        return Transition
                    .find(transitions, Transition.matcher(state, cell.read()))
                    .map(transition ->
                        env.update(cell.write(transition.write())
                                       .move(transition.move()),
                                   transition.toState()))
                    .orElse(null);
    }
    public Stream<Env> run(Env env) {
        return Stream.iterate(env, this::step).takeWhile(Objects::nonNull);
    }
}

List<Transition> r0 = List.of(
    new Transition("1",'1','1',Move.LEFT,"1"),
    new Transition("1",'0','1',Move.RIGHT,"2"),
    new Transition("2",'1','0',Move.RIGHT,"2"),
    new Transition("2",'0','1',Move.LEFT,"3"),
    new Transition("3",'0','0',Move.LEFT,"3"),
    new Transition("3",'1','1',Move.RIGHT,"4"),
    new Transition("4",'0','0',Move.LEFT,"H")
);

TM tm = new TM(r0);
String word = "01111110";
Cell cell = new Cell('_').init(word).slide(word.length(),Move.LEFT);
Env env = new Env(cell, "1");

List<Transition> trans1 = List.of(
    new Transition("1",'e','e',Move.RIGHT,"1"),
    new Transition("1",'1','1',Move.RIGHT,"1"),
    new Transition("1",'-','-',Move.RIGHT,"1"),
    new Transition("1",'=','e',Move.LEFT,"2"),
    new Transition("2",'1','=',Move.LEFT,"3"),
    new Transition("2",'-','e',Move.LEFT,"H"),
    new Transition("3",'1','1',Move.LEFT,"3"),
    new Transition("3",'-','-',Move.LEFT,"4"),
    new Transition("4",'e','e',Move.LEFT,"4"),
    new Transition("4",'1','e',Move.RIGHT,"1")   
);

TM tm1 = new TM(trans1);
String word1 = "111-1=";
Cell cell1 = new Cell('e').init(word1).slide(word1.length(),Move.LEFT);
Env env1 = new Env(cell1, "1");

List<Transition> adder = List.of(
    new Transition("S",'0','0',Move.LEFT,"0"),  // 9
    new Transition("0",'0','0',Move.LEFT,"00"), // 10
    new Transition("00",'0','0',Move.LEFT,"0"), // 11
    new Transition("00",'1','0',Move.RIGHT,"000"),  // 12 
    new Transition("000",'0','1',Move.LEFT,"S"), // 13
    new Transition("0",'1','1',Move.LEFT,"10"), // 14
    new Transition("10",'0','0',Move.LEFT,"0"), // 15
    new Transition("10",'1','1',Move.RIGHT,"110"),
    new Transition("110",'1','0',Move.LEFT,"S"), // 16

    new Transition("S",'1','1',Move.LEFT,"1"), // 19
    new Transition("1",'0','0',Move.LEFT,"01"), // 20
    new Transition("01",'0','0',Move.RIGHT,"001"), // 21
    new Transition("001",'0','1',Move.LEFT,"S"), // 22 correct
    new Transition("01",'1','1',Move.LEFT,"1"), // 23
    new Transition("1",'1','1',Move.LEFT,"11"), // 24
    new Transition("11",'0','1',Move.RIGHT,"111"), // 25
    new Transition("111",'1','0',Move.LEFT,"S"), // 26
    new Transition("11",'1','1',Move.LEFT,"1"),  // 27

    new Transition("0",'_','_',Move.RIGHT,"HALT"),
    new Transition("1",'_','_',Move.RIGHT,"HALT"),
    new Transition("S",'_','_',Move.RIGHT,"HALT")
);

TM tm = new TM(adder);
// String word = "000";
Cell cell = new Cell('_').init("100111").slide(1,Move.LEFT);
Env env = new Env(cell, "S");

/*
jshell> Cell cell = new Cell('_').init("100111").slide(1,Move.LEFT);
cell ==>  1  0  0  1  1 <1>

jshell> tm.run(new Env(cell, "0")).forEachOrdered(System.out::println)
  0:  1  0  0  1  1 <1> [0]
  1:  1  0  0  1 <1> 1  [10]
  2:  1  0  0  1  1 <1> [110]
  3:  1  0  0  1 <1> 0  [S]
  4:  1  0  0 <1> 1  0  [1]
  5:  1  0 <0> 1  1  0  [11]
  6:  1  0  1 <1> 1  0  [111]
  7:  1  0 <1> 0  1  0  [S]
  8:  1 <0> 1  0  1  0  [1]
  9: <1> 0  1  0  1  0  [01]
 10: <_> 1  0  1  0  1  0  [1]
 11: <1> 0  1  0  1  0  [HALT]
 */


