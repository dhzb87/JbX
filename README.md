# Javalin by Example (JbX) <!-- omit in toc -->

In diesem Repository geht es um die Programmierung von Javalin-Anwendungen als Grundlage für die Programmierung interaktiver Systeme. So wie Javalin sowohl für Java wie auch für Kotlin gedacht ist, bietet Ihnen diese Seite die Inhalte einmal mit Java und einmal mit Kotlin als Grundlage.

Sie sind herzlich eingeladen, mit weiteren Code-Beispielen zu diesem Repository beizutragen. Und wenn Sie Fehler finden und Korrekturvorschläge haben, auch solche Beiträge sind wertvoll.

- [Übersicht zu Javalin-Beispielen](#%c3%9cbersicht-zu-javalin-beispielen)
- [Wie benutzt man Gradle, Git und IntelliJ für Javalin Anwendungen?](#wie-benutzt-man-gradle-git-und-intellij-f%c3%bcr-javalin-anwendungen)
- [Aufgaben](#aufgaben)
- [Übersicht zu Beispielen mit einem modernen Java-Programmierstil](#%c3%9cbersicht-zu-beispielen-mit-einem-modernen-java-programmierstil)
- [Benötigte Software](#ben%c3%b6tigte-software)
- [Wie man mit dem Git-Repository `JbX` arbeitet](#wie-man-mit-dem-git-repository-jbx-arbeitet)

## Übersicht zu Javalin-Beispielen

Java-Versionen:

- [Ein einfaches Beispiel mit HTML-Forms](HTMLFormsExample) -- Das ist ein guter Einstieg!
- [Ein Spielfeld für Tic-Tac-Toe](TicTacToe.Spielfeld) -- Hier lernen Sie vieles, was Sie brauchen werden.
  
Kotlin-Versionen:

- [Ein einfaches Beispiel mit HTML-Forms](HTMLFormsExampleKotlin) -- Das ist ein guter Einstieg!
- [Ein Spielfeld für Tic-Tac-Toe](TicTacToe) -- Hier lernen Sie vieles, was Sie brauchen werden.

## Wie benutzt man Gradle, Git und IntelliJ für Javalin Anwendungen?

* [Gradle für Java-Projekte](GradleJava.md) -- Wie man ein Gradle-Projekt für Java-Programme anlegt
* [Gradle für Kotlin-Projekte](GradleKotlin.md) -- Wie man ein Gradle-Projekt für Kotlin-Programme anlegt 
* [Wie man Gradle und Git in IntelliJ benutzt](IntelliJ.md) -- Eine kurze, knappe Anleitung, wie man ein Projekt aus einem Git-Repository in IntelliJ importiert und ein Gradle-Projekt in IntelliJ richtig benutzt.

## Aufgaben

* [Aufgabe 1: Taschenrechner-UI](Aufgaben/Aufgabe1.md)
* [Aufgabe 2: Währungsrechner](Aufgaben/Aufgabe2.md)
* [Aufgabe 3: Zahleneingabe für Taschenrechner](Aufgaben/Aufgabe3.md)
* [Aufgabe 4: Qualifizierung im Team](Aufgaben/Aufgabe4.md) (nicht relevant im SoSe2020)
* [Infos zur Abgabe von Aufgabe 4](Aufgaben/Aufgabe4.Abgabe.md) (nicht relevant im SoSe2020)

## Übersicht zu Beispielen mit einem modernen Java-Programmierstil

Java hat sich in den letzten Jahren sehr gewandelt, moderne und modernisierte Sprachkonstrukte sind hinzugekommen, die vor allem einen eher funktional-orientierten Programmierstil erlauben.

Hier finden Sie Beispiele zum Nachschlagen und Lernen. Zu jedem Beispiel sind die wesentlichen Programmkonstrukte gelistet mit dem ein oder anderen Hinweis. Beachten Sie: Die Beispiele sind vollkommen unabhängig von Javalin.

* [Der kleine Gauß: Von 1 bis 100](ModernJava/Gauss.md): `IntStream`, `rangeClosed()`, `sum()`, `reduce()`, `peek()`, Methodenreferenzen `System.out::println`, `Integer::sum`
* [Primzahl-Würfel](ModernJava/PrimzahlWuerfel.md): `List.of()`, `flatMap()`, `filter()`, `count()`, `map()`, `Stream<Integer>`, `IntStream`, `Arrays.stream()`, Trick für mehrfache Nutzung eines Streams
* [Quicksort mit Streams](ModernJava/Quicksort.md): `IntStream.Builder`, `builder()`, `build()`, `reduce()` mit Seiteneffekt, `IntStream.empty()`, `IntStream.concat()`, `IntStream.of()`, `OptionalInt`, `isEmpty()`, `getAsInt()`
* [Turing-Maschine](ModernJava/turing.java): `record`, `switch`-Ausdruck, `Objects.nonNull()`, `Predicate<>`, `Function<>`, `filter()`, `find()`, `iterate()`, `map()`, `reduce()`, `takeWhile()`,  `Optional<>`, `orElse()`, `List.of()`
* [Sortieren einer Liste](https://dzone.com/articles/java-8-comparator-how-to-sort-a-list) (externer Link): Hier lernen Sie den Einsatz eines `Comparator`s. Wenn Sie alle vorgestellten Möglichkeiten verstehen, wie man die Sortierung einer Liste konfigurieren kann, dann sind Sie schon sehr weit in Ihrem Java-Verständnis gekommen. 
* Markdown-Excel (externer Link, Code ist von mir): Die Idee von Markdown-Excel können Sie [hier](https://gist.github.com/denkspuren/c91b4dd9ffcc4c6040ce271e3fd7caa9) nachlesen. Dazu ist ein [Demo-Code](https://gist.github.com/denkspuren/5b1a048bce056c478794007a1467377b) entstanden, der mithilfe von Lambda-Ausdrücken und Streams eine sehr elegante Bearbeitung von Tabellen erlaubt. Der Demo-Code ist eine Konzeptstudie und geht davon aus, dass das Preprocessing, also das Einlesen der Tabellen eines Markdown-Dokument, und das Postprocessing, das Erzeugen eines aktualisierten Markdown-Dokuments, gelöst sind.

## Benötigte Software

Sie müssen, wenn Sie in Java bzw. Kotlin programmieren und Javalin-Anwendungen schreiben wollen, einige Werkzeuge auf Ihrem Rechner installiert haben. Hier die Auswahl der Programme, die ich auf meinem Rechner installiert habe:

* Das aktuelle JDK (Java Development Kit); ich verwende das OpenJDK
* Für Java einen Programmeditor wie [Visual Studio Code](https://code.visualstudio.com/) (VSC); auch der Editor [Atom](http://atom.io) ist beliebt
  - Die Erweiterung "Java Extension Pack" von Microsoft sollten Sie unbedingt für VSC installieren
* Für Kotlin ist die Installation der Entwicklungsumgebung [IntelliJ IDEA](https://www.jetbrains.com/de-de/idea/) praktisch alternativlos. Kotlin ist eine Entwicklung der Firma IntelliJ, Sie haben dort die beste, denkbare Sprachunterstützung. Sie bekommen eine kostenlose Lizenz für die Ultimate-Edition, wenn Sie diese über Ihre Hochschul-Emailadresse auf der Jetbrains-Website beantragen.
* Das Build-Werkzeuge gradle
* Das Versionsverwaltungsprogramm git
* Das Kommandozeilenwerkzeug curl

Für die Installation dieser Programme (mit Ausnahme von Visual Studio Code und IntelliJ IDEA) empfehle ich Ihnen auf Windows, Scoop zu nehmen. Problemloser geht es nicht.

* [Installation von Software mit Scoop](Scoop.md)

## Wie man mit dem Git-Repository `JbX` arbeitet

Dieses Repository _Javalin by Example_ (JbX) ist innerhalb der THM für Sie zugänglich unter der Adresse:

https://git.thm.de/dhzb87/JbX

Sie können sich das Repository (Repo) als gepackte Datei herunterladen. Aber am besten nutzen Sie git, indem Sie das Repo `clonen`.

    git clone https://git.thm.de/dhzb87/JbX.git

Wenn Sie das Dateien in Ihrem lokalen `JbX`-Verzeichnis mit dem Repo auf den neuesten Stand bringen wollen, _pullen_ (herunterziehen) sich die aktuellste Fassung.

    git pull

Wechsel Sie in das Verzeichnis, das Sie interessiert. Liegt dort eine Datei namens `build.gradle`, können Sie die Anwendung übersetzen und ausführen mit

    gradle run
---

Quellennachweis für das "Logo": [Pixabay](https://pixabay.com/de/illustrations/geschenke-hintergrund-bunte-box-1913987/) (Zugriff: 21.3.2019)